﻿using String_Calculator_2.Interface;
using System;

namespace String_Calculator_2
{
    public class DelimiterExtractor : IDelimiterExtractor
    {
        public string CustomDelimiterId { get; set; }
        private readonly string _newline = "\n";
       
        public string[] GetDelimiters(string orginialNumberString, string customDelimiter)
        {
            CustomDelimiterId = customDelimiter;
            var splitter = GetDelimiterSplitter(orginialNumberString);
            var delimiterSection = GetDelimitersSection(orginialNumberString);

            return GetSortedDelimiters(GetDelimiters(delimiterSection, splitter));
        }

        public string[] GetDelimiters(string delimiterSection, string[] delimiterSplitter)
        {
            var multipleCustomDelimitersSplitter = delimiterSplitter[1] + delimiterSplitter[0];

            if (string.IsNullOrEmpty(delimiterSection))
            {
                return new[] { ",", _newline };
            }

            if (delimiterSection.Contains(multipleCustomDelimitersSplitter))
            {
                var delimiters = delimiterSection.Remove(0, 1).Remove(delimiterSection.Length - 2, 1);

                return delimiters.Split(delimiterSplitter, StringSplitOptions.RemoveEmptyEntries);
            }

            if (delimiterSection.StartsWith(delimiterSplitter[0]))
            {
                delimiterSection = delimiterSection.Remove(0, 1).Remove(delimiterSection.Length - 2, 1);
            }

            return new[] { delimiterSection };
        }

        public string GetDelimitersSection(string numbers)
        {
            if (numbers.StartsWith(CustomDelimiterId) || numbers.StartsWith("<"))
            {
                var delimiterStartIndex = numbers.IndexOf(CustomDelimiterId) + CustomDelimiterId.Length;
                var delimiterEndIndex = numbers.IndexOf(_newline);

                return numbers.Substring(delimiterStartIndex, (delimiterEndIndex - delimiterStartIndex));
            }

            return string.Empty;
        }

        public string[] GetDelimiterSplitter(string numbers)
        {
            var customSplitterIdentifier = new[] { "<", ">" };

            if (numbers.StartsWith("<"))
            {
                return new[] { numbers.Substring(numbers.IndexOf(customSplitterIdentifier[0]) + 1, numbers.IndexOf(customSplitterIdentifier[1]) - 1), numbers.Substring(numbers.IndexOf(">") + 1, numbers.IndexOf(CustomDelimiterId) - 3) };
            }

            return new[] { "[", "]" };
        }

        public string[] GetSortedDelimiters(string[] delimiters)
        {
            Array.Sort(delimiters, (x, y) => y.Length.CompareTo(x.Length));

            return delimiters;
        }
    }
}
