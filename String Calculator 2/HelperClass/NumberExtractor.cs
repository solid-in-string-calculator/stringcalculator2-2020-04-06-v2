using String_Calculator_2.Interface;
using System;
using System.Collections.Generic;

namespace String_Calculator_2
{
    public class NumberExtractor : INumberExtractor
    {
        public List<int> GetNumbers(string[] delimiters, string numbers, string customDelimiterId)
        {
            var numbersSection = GetNumbersSection(numbers,customDelimiterId);
            var numbersArray = GetNumbersArray(numbersSection, delimiters);
            
            return GetNumbers(numbersArray);
        }

        public string[] GetNumbersArray(string numbersSection, string[] delimiters)
        {
            var acceptedAlphabets = new[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j" };

            var numbersArray = numbersSection.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < numbersArray.Length; i++)
            {
                numbersArray[i] = numbersArray[i].Replace(acceptedAlphabets[i], i.ToString());
            }

            return numbersArray;
        }

        public static void ValidateNumbers(List<int> numbersList, string message, int flag)
        {
            List<string> invalidNumbers = new List<string>();

            foreach (var number in numbersList)
            {
                if (flag == 1000 && number > flag)
                {
                    invalidNumbers.Add(number.ToString());
                }

                if (flag == 0 && number < flag)
                {
                    invalidNumbers.Add(number.ToString());
                }
            }

            if (invalidNumbers.Count != 0)
            {
                throw new Exception(message + string.Join(", ", invalidNumbers));
            }
        }

        public string GetNumbersSection(string numbers, string customDelimiterId)
        {
            if (numbers.StartsWith(customDelimiterId) || numbers.StartsWith("<"))
            {
                return numbers.Substring(numbers.IndexOf("\n") + 1);
            }

            return numbers;
        }

        public List<int> GetNumbers(string[] numbersArray)
        {
            List<int> numbersList = new List<int>();

            foreach (var num in numbersArray)
            {
                if (int.TryParse(num, out int number))
                {
                    numbersList.Add(number);
                }
            }

            return numbersList;
        }
    }
}
