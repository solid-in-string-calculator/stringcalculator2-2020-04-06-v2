﻿using System.Collections.Generic;

namespace String_Calculator_2.Interface
{
    public interface INumberExtractor
    {
        List<int> GetNumbers(string[] numbersArray, string numbers, string customDelimiterId);
    }
}
