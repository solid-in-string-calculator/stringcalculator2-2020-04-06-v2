﻿namespace String_Calculator_2.Interface
{
    public interface ICalculator
    {
        int Add(string numbers);

        int Subtract(string numbers);
    }
}
