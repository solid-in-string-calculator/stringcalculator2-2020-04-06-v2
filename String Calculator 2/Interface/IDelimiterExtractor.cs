﻿namespace String_Calculator_2.Interface
{
    public interface IDelimiterExtractor
    {
        string[] GetDelimiters(string orginialNumberString, string customDelimiter);
    }
}
