using String_Calculator_2.Interface;
using System;
using System.Collections.Generic;

namespace String_Calculator_2
{
    public class Calculator : ICalculator
    {
        private readonly INumberExtractor _numberExtractor;
        private readonly IDelimiterExtractor _delimiterExtractor;

        public Calculator(INumberExtractor number, IDelimiterExtractor delimiter)
        {
            _numberExtractor = number;
            _delimiterExtractor = delimiter;
        }

        public int Add(string numbers)
        {
            var delimiters = _delimiterExtractor.GetDelimiters(numbers, "//");
            var numbersList = _numberExtractor.GetNumbers(delimiters, numbers, "//");

            NumberExtractor.ValidateNumbers(numbersList, "Negative not allowed ", 0);

            return Calculate(numbersList, "+");
        }

        public int Subtract(string numbers)
        {
            var delimiters = _delimiterExtractor.GetDelimiters(numbers, "##");
            var numbersList = _numberExtractor.GetNumbers(delimiters, numbers, "##");

            NumberExtractor.ValidateNumbers(numbersList, "Numbers above a thousand are not allowed ", 1000);

            return Calculate(numbersList, "-");
        }

        public int Calculate(IEnumerable<int> numbersList, string operation)
        {
            var total = 0;

            foreach (var number in numbersList)
            {
                if (operation.Equals("+"))
                {
                    if (number < 1001)
                    {
                        total += number;
                    }
                }
                else if (operation.Equals("-"))
                {
                    total += -Math.Abs(number);
                }
            }

            return total;
        }
    }
}
