﻿using NUnit.Framework;
using String_Calculator_2;

namespace StringCalculator2.Test
{
    [TestFixture]
    public class TestDelimiterExtractor
    {
        private DelimiterExtractor _delimiterExtractor;

        [OneTimeSetUp]
        public void Init()
        {
            _delimiterExtractor = new DelimiterExtractor();
        }

        [Test]
        public void GIVEN_DelimiterSectionAndDelimiterSplitterWithNoCustomDelimiters_WHEN_GettingDelimiters_THEN_ReturnDelimiters()
        {
            var delimiterSection = string.Empty;
            var delimiterSplitter = new[] { "[", "]" };
            var expected = new[] { ",", "\n" };
            var actual = _delimiterExtractor.GetDelimiters(delimiterSection, delimiterSplitter);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_DelimiterSectionAndDelimiterSplitterWithMultipleCustomDelimiters_WHEN_GettingDelimiters_THEN_ReturnDelimiters()
        {
            var delimiterSection = "[*][.]";
            var delimiterSplitter = new[] { "[", "]" };
            var expected = new[] { "*", "." };
            var actual = _delimiterExtractor.GetDelimiters(delimiterSection, delimiterSplitter);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_DelimiterSectionAndDelimiterSplitterWithSingleCustomDelimiters_WHEN_GettingDelimiters_THEN_ReturnDelimiters()
        {
            var delimiterSection = "*";
            var delimiterSplitter = new[] { "[", "]" };
            var expected = new[] { "*"};
            var actual = _delimiterExtractor.GetDelimiters(delimiterSection, delimiterSplitter);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_DelimiterSectionAndDelimiterSplitterWithSingleCustomDelimiterAnd_WHEN_GettingDelimiters_THEN_ReturnDelimiters()
        {
            var delimiterSection = "(*)";
            var delimiterSplitter = new[] { "(", ")" };
            var expected = new[] { "*" };
            var actual = _delimiterExtractor.GetDelimiters(delimiterSection, delimiterSplitter);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NumbersWithCustomDelimiter_WHEN_GettingDelimiterSection_THEN_ReturnDelimiterSection()
        {
            var numbers = "//[**][..]\n1**3..6";
            var expected = "[**][..]";
            _delimiterExtractor.CustomDelimiterId = "//";
            var actual = _delimiterExtractor.GetDelimitersSection(numbers);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NumbersWithCustomDelimiterSplitter_WHEN_GettingDelimiterSection_THEN_ReturnDelimiterSection()
        {
            var numbers = "<{>)##{*)\n2*4*6";
            var expected = "{*)";
            _delimiterExtractor.CustomDelimiterId = "##";
            var actual = _delimiterExtractor.GetDelimitersSection(numbers);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NumbersWithDefaultDelimiter_WHEN_GettingDelimiterSection_THEN_ReturnEmptyString()
        {
            var numbers = "1,5\n6";
            var expected = string.Empty;
            _delimiterExtractor.CustomDelimiterId = "##";
            var actual = _delimiterExtractor.GetDelimitersSection(numbers);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NumbersWithCustomDelimiterSplitter_WHEN_GettingDelimiterSplitter_THEN_ReturnDelimiterSplitter()
        {
            var numbers = "<{>)##{*){.)\n2*4.6";
            var expected = new[] { "{", ")" };
            _delimiterExtractor.CustomDelimiterId = "##";
            var actual = _delimiterExtractor.GetDelimiterSplitter(numbers);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NumbersWithDefaultDelimiterSplitter_WHEN_GettingDelimiterSplitter_THEN_ReturnDefaultDelimiterSplitter()
        {
            var numbers = "##[*][.]\n2*4.6";
            var expected = new[] { "[", "]" };
            _delimiterExtractor.CustomDelimiterId = "##";
            var actual = _delimiterExtractor.GetDelimiterSplitter(numbers);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_Delimiters_WHEN_GettingSortedDelimiters_ReturnDelimiters()
        {
            var delimiters = new[] { "$", "$$F", "$F", "F", "$$" };
            var expected = new[] { "$$F", "$F", "$$", "$", "F" };
            var actual = _delimiterExtractor.GetSortedDelimiters(delimiters);

            Assert.AreEqual(expected, actual);
        }
    }
}
