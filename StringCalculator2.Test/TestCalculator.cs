﻿using NSubstitute;
using NUnit.Framework;
using String_Calculator_2;
using String_Calculator_2.Interface;
using System.Collections.Generic;

namespace StringCalculator2.Test
{
    [TestFixture]
    public class TestCalculator
    {
        private Calculator _calculator;
        private INumberExtractor _numbersExtractor;
        private IDelimiterExtractor _delimiterExtractor;

        [OneTimeSetUp]
        public void Init()
        {
            _numbersExtractor = Substitute.For<INumberExtractor>();
            _delimiterExtractor = Substitute.For<IDelimiterExtractor>();
            _calculator = new Calculator(_numbersExtractor, _delimiterExtractor);
        }

        [Test]
        public void GIVEN_Numbers_WHEN_Adding_THEN_ReturnSum()
        {
            _delimiterExtractor.GetDelimiters(Arg.Any<string>(), Arg.Any<string>()).Returns(new[] { ",", "\n" });
            _numbersExtractor.GetNumbers(Arg.Any<string[]>(), Arg.Any<string>(), Arg.Any<string>()).Returns(new List<int> { 1, 2, 3, 4, 5 });

            var expected = 15;
            var actual = _calculator.Add("1,2,3,4,5");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_Numbers_WHEN_Subtracting_THEN_ReturnDifference()
        {
            _delimiterExtractor.GetDelimiters(Arg.Any<string>(), Arg.Any<string>()).Returns(new[] { ",", "\n" });
            _numbersExtractor.GetNumbers(Arg.Any<string[]>(), Arg.Any<string>(), Arg.Any<string>()).Returns(new List<int> { 1, 2, 3, 4, 5 });

            var expected = -15;
            var actual = _calculator.Subtract("1,2,3,4,5");

            Assert.AreEqual(expected, actual);
        }
    }
}
