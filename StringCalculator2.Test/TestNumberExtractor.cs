﻿using NUnit.Framework;
using String_Calculator_2;
using System;
using System.Collections.Generic;

namespace StringCalculator2.Test
{
    [TestFixture]
    public class TestNumberExtractor
    {
        private NumberExtractor _numberExtractor;

        [OneTimeSetUp]
        public void Init()
        {
            _numberExtractor = new NumberExtractor();
        }

        [Test]
        public void GIVEN_NumbersSectionAndDelimiters_WHEN_GettingNumbersArray_THEN_ReturnNumbersArray()
        {
            var numbersSection = "4;6#9.8*10";
            var delimiters = new[] { ";", "#", ".", "*" };
            var expected = new[] { "4", "6", "9", "8", "10" };
            var actual = _numberExtractor.GetNumbersArray(numbersSection, delimiters);

            Assert.AreEqual(expected, actual);
        }



        [Test]
        public void GIVEN_NumbersWithLettersAndDelimitersWithLetters_WHEN_GettingNumbersArray_THEN_ReturnNumbersArraySubstitutedCorrectly()
        {
            var numbersSection = "aab6ab9ab8ab10";
            var delimiters = new[] { "ab" };
            var expected = new[] { "0", "6", "9", "8", "10" };
            var actual = _numberExtractor.GetNumbersArray(numbersSection, delimiters);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NumbersListMessageAndFlag_WHEN_ValidatingNegativeNumbers_THEN_ThrowException()
        {
            var numbersList = new List<int> { 45, -7, 23, 68, -4 };
            var message = "Negative number not allowed ";
            int flag = 0;
            var expected = $"{message}-7, -4";
            var actual = Assert.Throws<Exception>(() => NumberExtractor.ValidateNumbers(numbersList, message, flag));

            Assert.AreEqual(expected, actual.Message);
        }

        [Test]
        public void GIVEN_NumbersListAndMessageAndFlag_WHEN_ValidatingNumbersAboveThousand_THEN_ThrowException()
        {
            var numbersList = new List<int> { 45, 1056, 23, 68, -4 };
            var message = "Numbers above a thousand are not allowed ";
            int flag = 1000;
            var expected = $"{message}1056";
            var actual = Assert.Throws<Exception>(() => NumberExtractor.ValidateNumbers(numbersList, message, flag));

            Assert.AreEqual(expected, actual.Message);
        }

        [Test]
        public void GIVEN_NumbersWithCustomDelimiter_WHEN_GettingNumberSection_THEN_ReturnDelimiterSection()
        {
            var numbers = "//[**][..]\n1**3..6";
            var customDelimiterId = "//";
            var expected = "1**3..6";
            var actual = _numberExtractor.GetNumbersSection(numbers, customDelimiterId);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NumbersWithCustomDelimiterSplitter_WHEN_GettingDelimiterSection_THEN_ReturnDelimiterSection()
        {
            var numbers = "<{>)##{*)\n2*4*6";
            var customDelimiterId = "//";
            var expected = "2*4*6";
            var actual = _numberExtractor.GetNumbersSection(numbers, customDelimiterId);

            Assert.AreEqual(expected, actual);
        }
        

        [Test]
        public void GIVEN_NumbersWithDefaultDelimiter_WHEN_GettingDelimiterSection_THEN_ReturnNumbers()
        {
            var numbers = "1,5\n6";
            var customDelimiterId = "//";
            var expected = numbers;
            var actual = _numberExtractor.GetNumbersSection(numbers, customDelimiterId);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NumbersArray_WHEN_GettingNumbers_THEN_ReturnNumberList()
        {
            var numbersArray = new[] { "4", "6", "9", "8", "10" };
            var expected = new List<int> { 4, 6, 9, 8, 10 };
            var actual = _numberExtractor.GetNumbers(numbersArray);

            Assert.AreEqual(expected, actual);
        }
    }
}
